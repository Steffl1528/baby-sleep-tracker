import { type ICurrentTrackedTime, type ITrackedTime } from './trackedTime'

const DB_NAME = 'SleepDB'
const OBJECT_STORE_NAME = 'trackedTimes'
const DB_VERSION = 1
let db: IDBDatabase

export async function openDb() {
  return new Promise((resolve, reject): void => {
    const connection = indexedDB.open(DB_NAME, DB_VERSION)

    connection.onerror = (e) => {
      reject('Error opening db')
    }

    connection.onsuccess = (e) => {
      db = (e.target as IDBOpenDBRequest).result
      resolve(db)
    }

    connection.onupgradeneeded = (e) => {
      const db = (e.target as IDBOpenDBRequest).result
      const objectStore = db.createObjectStore(OBJECT_STORE_NAME, {
        autoIncrement: true,
        keyPath: 'id'
      })

      objectStore.createIndex('hours', 'hours')
      objectStore.createIndex('minutes', 'minutes')
      objectStore.createIndex('seconds', 'seconds')
      objectStore.createIndex('isSleepTime', 'isSleepTime')
      objectStore.createIndex('startTime', 'startTime')
      objectStore.createIndex('endTime', 'endTime')
    }
  })
}

export async function initDb() {
  if (!db) {
    await openDb()
  }
}

export function getTrackedTimesFromDb() {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction([OBJECT_STORE_NAME], 'readonly')
    const store = transaction.objectStore(OBJECT_STORE_NAME)
    const request = store.getAll()

    request.onerror = (e) => {
      console.error('Error reading data from db', e)
      reject(e)
    }

    request.onsuccess = (e) => {
      resolve((e.target as IDBRequest).result)
    }
  })
}

export async function addTrackedTimeToDb(
  trackedTime: ICurrentTrackedTime
): Promise<any> {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction([OBJECT_STORE_NAME], 'readwrite')

    const store = transaction.objectStore(OBJECT_STORE_NAME)

    const request = store.add(trackedTime)

    request.onerror = (e) => {
      console.error('Error writing data to db', e)
      reject(e)
    }

    request.onsuccess = (e) => {
      resolve((e.target as IDBRequest).result)
    }
  })
}

export async function deleteAllTrackedTimesFromDb() {
  const transaction = db.transaction([OBJECT_STORE_NAME], 'readwrite')

  const store = transaction.objectStore(OBJECT_STORE_NAME)

  const request = store.clear()

  request.onerror = (e) => {
    console.error('Error deleting data to db', e)
  }

  request.onsuccess = (e) => {}
}

export async function deleteTrackedTimeByIdFromDb(id: number | null) {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction([OBJECT_STORE_NAME], 'readwrite')

    const store = transaction.objectStore(OBJECT_STORE_NAME)

    if (id) {
      const request = store.delete(id)

      request.onerror = (e) => {
        reject(false)
      }

      request.onsuccess = (e) => {
        resolve(true)
      }
    } else {
      reject(false)
    }
  })
}
