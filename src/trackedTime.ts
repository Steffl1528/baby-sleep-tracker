export interface ITrackedTime {
  hours: number;
  minutes: number;
  seconds: number;
  isSleepTime?: boolean;
  startTime?: string;
  endTime?: string;
  id: number | null;
  isEditable?: boolean;
}

export interface ICurrentTrackedTime {
  hours: number;
  minutes: number;
  seconds: number;
  isSleepTime?: boolean;
  startTime?: string;
  endTime?: string;
}
