import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

export default defineConfig({
  plugins: [
    vue(),
     VitePWA({
       registerType: 'autoUpdate',
       devOptions: { enabled: true },
       manifest: {
        name: 'Baby Sleep Tracker',
        theme_color: '#85DAEC',
        icons: [
          {
            "src": "@assets/baby-32x32.png",
            "type": "image/png",
            "sizes": "32x32"
          },
          {
            "src": "@assets/baby-64x64.png",
            "type": "image/png",
            "sizes": "64x64"
          },
          {
            "src": "@assets/baby-192x192.png",
            "type": "image/png",
            "sizes": "192x192"
          },
          {
            "src": "@assets/baby-256x256.png",
            "type": "image/png",
            "sizes": "256x256"
          },
          {
            "src": "@assets/baby-512x512.png",
            "type": "image/png",
            "sizes": "512x512"
          },
        ]
       }
     })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@assets': fileURLToPath(new URL('./assets', import.meta.url)),
    }
  }
})
